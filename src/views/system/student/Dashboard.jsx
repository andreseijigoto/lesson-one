import React, { Component, Fragment } from 'react'
import Columns from 'react-bulma-components/lib/components/columns'
import Panel from '$components/Panel.jsx'

import WorksModel from '$models/Works.jsx'

export default class Dashboard extends Component
{
  constructor()
  {
    super()
    this.state = { works: [] }
  }
  componentDidMount()
  {
    WorksModel.getLatest( data => this.setState( { works:data } ) )
  }
  render()
  {
    return (
      <Fragment>
        <Columns>
          <Columns.Column size={"half"}>
            <Panel title="Cadastro incompleto" header={{icon:"exclamation"}} className="danger" items={
            [
              { path: '#', label: 'Confirmação de E-mail' },
              { path: '#', label: 'Comprovante de Curso (Graduação)' },
              { path: '#', label: 'Comprovante de Endereço' }
            ]}/>
          </Columns.Column>
          <Columns.Column size={"half"}>
            <Panel title="Conheça o sistema" header={{icon:"question"}} className="alert" items={
            [
              { path: '#', label: 'Processos de contratação e estágio' },
              { path: '#', label: 'Abrangência de responsabilidades' },
              { path: '#', label: 'Escolas e Locais conveniados' }
            ]}/>
          </Columns.Column>
        </Columns>
        <Panel header={{title:"Vagas"}} items={this.state.works}/>
      </Fragment>
    )
  }
}