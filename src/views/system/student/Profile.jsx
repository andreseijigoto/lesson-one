import React, { Component } from 'react'

import Button from 'react-bulma-components/lib/components/button'
import Columns from 'react-bulma-components/lib/components/columns'
import Heading from 'react-bulma-components/lib/components/heading'
import Notification from 'react-bulma-components/lib/components/notification'

import Input from '$components/Input.jsx'

import UsersModel from '$models/Users.jsx'

import '$sass/views/profile.scss'

export default class Profile extends Component
{
  constructor()
  {
    super()
    this.state = {}
    this.saveProfile = this.saveProfile.bind( this )
    this.userId = sessionStorage.getItem( 'userId' )
  }
  saveProfile( e )
  {
    e.preventDefault()
    const fieldList = e.target.elements
    let dataSend = {}
    Object.keys( fieldList ).map( key => {
      dataSend[ fieldList[ key ].name ] = fieldList[ key ].value
      if( fieldList[ key ].type === 'file' )
        dataSend[ fieldList[ key ].name ] = fieldList[ key ].files[0]        
      return null
    })
    UsersModel.setProfile( this.userId, dataSend, res => {
      if( res.success )
        this.setState({message:'Dados do perfil salvos com sucesso!'})
      if( res.error )
        this.setState({error:'Não foi possivel salvar os dados. Contacte o Suporte para maiores informações.'})
      this.componentDidMount()
    })
  }
  componentDidMount()
  {
    UsersModel.getDataById( this.userId, res => {
      console.log( res )
      if( res.photo )
        document.querySelector( '#profile-photo' ).parentElement.style.backgroundImage = res.photo
      this.setState( res )
    })
  }
  render()
  {
    return (
      <div id="Profile">
        {
          this.state.message || this.state.error ?
          <Notification color={this.state.message ? 'info' : 'danger'}>
          {this.state.message || this.state.error}<Button remove onClick={ e =>{ this.setState( { error:null } ) } }/>
          </Notification>
          : ''
        }
        <Heading renderAs="h1">Dados Pessoais</Heading>
        <form id="ProfileForm" onSubmit={this.saveProfile}>
          <Columns>
            <Columns.Column size={3}>
              <Input type="file" name="photo" id="profile-photo" label="Foto" backgroundImage={this.state.photo}/>
            </Columns.Column>
            <Columns.Column size={9}>
              <Input size="large" type="text" name="name" id="profile-name" label="Nome Completo" required value={this.state.name}/>
              <Columns>
                <Columns.Column size={4}>
                  <Input mask="99.999.999-?" size="large" type="text" name="rg" id="profile-rg" label="RG" required value={this.state.rg}/>
                </Columns.Column>
                <Columns.Column size={4}>                
                  <Input mask="999.999.999-99" size="large" type="text" name="cpf" id="profile-cpf" label="CPF" required value={this.state.cpf}/>
                </Columns.Column>
                <Columns.Column size={4}>
                  <Input mask="(99) ?9999-9999" size="large" type="text" name="phone" id="profile-phone" label="Telefone" required value={this.state.phone}/>
                </Columns.Column>
              </Columns>
              <Input size="large" type="email" name="email" id="profile-email" label="Email Completo" required value={this.state.email}/>
            </Columns.Column>
            <Columns.Column size={9}>
              <Input size="large" type="text" name="address" id="profile-address" label="Endereço" required value={this.state.address}/>
            </Columns.Column>
            <Columns.Column size={3}>
              <Input size="large" type="number" name="number" id="profile-number" label="Número" required value={this.state.number}/>
            </Columns.Column>
            <Columns.Column size={5}>
              <Input size="large" type="text" name="complement" label="Complemento" id="profile-complement" value={this.state.complement}/>
            </Columns.Column>
            <Columns.Column size={2}>
              <Input mask="99999-999" size="large" type="text" name="cep" id="profile-cep" label="CEP" required value={this.state.cep}/>
            </Columns.Column>
            <Columns.Column size={4}>
              <Input size="large" type="text" name="city" id="profile-city" label="Cidade" required value={this.state.city}/>
            </Columns.Column>
            <Columns.Column size={1}>
              <Input size="large" type="text" name="state" id="profile-state" label="Estado" required value={this.state.state}/>
            </Columns.Column>
          </Columns>
          <Button name="save" className="is-info is-large is-pulled-right">Salvar</Button>
        </form>
      </div>
    )
  }
}