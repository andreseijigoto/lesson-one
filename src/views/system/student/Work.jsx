import React, { Component } from 'react'
import { createBrowserHistory } from "history"
import Button from 'react-bulma-components/lib/components/button'
import Columns from 'react-bulma-components/lib/components/columns'
import Heading from 'react-bulma-components/lib/components/heading'
import Notification from 'react-bulma-components/lib/components/notification'
import Tile from 'react-bulma-components/lib/components/tile'

import WorksModel from '$models/Works.jsx'
import CandidacyModel from '$models/Candidacy.jsx'

import '$sass/views/work.scss'

export default class Work extends Component
{
  constructor()
  {
    super()
    this.state = { canBeApply: true }
    this.history = new createBrowserHistory()
    this.userId = sessionStorage.getItem( 'userId' )
    this.workId = this.history.location.pathname.split( '/' )[ 3 ]
    this.setWorkCandidacy = this.setWorkCandidacy.bind( this )
  }
  setWorkCandidacy()
  {
    WorksModel.getDocById( this.workId, workDoc => {
      let workRef = {}
      workRef[ this.workId ] = workDoc
      CandidacyModel.setCandidacyApply( this.userId, workRef, res => {
        if( res.success )
          this.setState({message:'Candidatura enviada com sucesso!'})
        if( res.error )
          this.setState({error:'Não foi possivel se candidatar a vaga. Contacte o Suporte para maiores informações.'})
      })
    })
  }
  componentDidMount()
  {
    WorksModel.getDataById( this.workId, res => {
      this.setState( res )
    })
    CandidacyModel.getCandidacyApplied( this.userId, this.workId, res => {
      this.setState({canBeApply:res})
    })
    let menuGroup = document.querySelector( '#MenuWorks' )
    let menuList = menuGroup.parentElement.querySelector( 'ul' )
    menuGroup.classList.add( 'active' )
    menuList.classList.remove( 'slideup' )
    menuList.classList.add( 'slidedown' )
    menuList.querySelector( `[href="${this.props.config.path}"]` ).style.display = 'block'
    menuList.querySelector( `[href="${this.props.config.path}"]` ).classList.add( 'active' )
  }
  render()
  {
    let img = this.state.image ? require( '$images/schools/' + this.state.image ) : null
    return (
      <div id="WorkDescription">
        {
          this.state.message || this.state.error ?
          <Notification color={this.state.message ? 'info' : 'danger'}>
          {this.state.message || this.state.error}<Button remove onClick={ e =>{ this.setState( { error:null } ) } }/>
          </Notification>
          : ''
        }
        <Columns>
          <Columns.Column size={5}>
          { img ? <img src={img} alt={this.state.name} /> : null }
          </Columns.Column>
          <Columns.Column size={6} offset={1}>
            <Heading className="has-text-weight-bold">
              {this.state.name}
            </Heading>
            <Heading renderAs="h2" subtitle className="is-size-4">
              Telefone: <a href={`tel:${this.state.phone}`} target="blank">{this.state.phone}</a>
            </Heading>
            <Heading renderAs="h2" subtitle className="is-size-4">
              Site: <a href={this.state.site} target="blank">{this.state.site}</a>
            </Heading>
          </Columns.Column>
        </Columns>
        <Tile kind="ancestor">
          <Tile kind="parent" vertical>
            <Tile kind="child">
              <Heading renderAs="h3" className="is-size-4">Descrição da Vaga</Heading>
              <div className="is-size-5">{this.state.description}</div>
            </Tile>
            <Tile kind="child">
              <Heading renderAs="h3" className="is-size-4">Requisitos</Heading>
              <div className="is-size-5">{this.state.conditions}</div>
            </Tile>
            <Tile kind="child">
              <Heading renderAs="h3" className="is-size-4">Beneficios</Heading>
              <div className="is-size-5">{this.state.benefits}</div>
            </Tile>
            <Tile kind="child">
              <Heading renderAs="h3" className="is-size-4">Horas Estágio</Heading>
              <div className="is-size-5">{this.state.teachHour}</div>
            </Tile>
          </Tile>
        </Tile>
        <Button name="search" className="has-background-grey-lighter is-large" onClick={this.history.goBack}>Voltar</Button>
        <Button name="search" className="is-info is-large is-pulled-right" disabled={!this.state.canBeApply} onClick={this.setWorkCandidacy}>Candidatar</Button>
      </div>
    )
  }
}
