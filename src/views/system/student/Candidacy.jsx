import React, { Component } from 'react'

import Box from 'react-bulma-components/lib/components/box'
import Content from 'react-bulma-components/lib/components/content'
import Image from 'react-bulma-components/lib/components/image'
import Level from 'react-bulma-components/lib/components/level'
import Media from 'react-bulma-components/lib/components/media'
import Heading from 'react-bulma-components/lib/components/heading'

import CandidacyModel from '$models/Candidacy.jsx'

import '$sass/views/candidacy.scss'

export default class Candidacy extends Component
{
  constructor()
  {
    super()
    this.state = { candidacies:[] }
    this.userId = sessionStorage.getItem( 'userId' )
    this.candidacies = []
  }
  getMessage( count )
  {
    if( count )
      return 'Você está inscrito no(s) processo(s) qualificatório das seguintes vagas:'
    return 'Você ainda não se inscreveu para nenhuma vaga'
  }
  async componentDidMount()
  {
    CandidacyModel.getCadidacyList( this.userId, candidacies => {
      this.setState({candidacies:candidacies})
    })
  }
  render()
  {
    console.log( this.state )
    return (
      <div id="Candidacy">
        <Heading renderAs="h1">Candidaturas</Heading>
        <div>{this.getMessage( this.state )}</div>
        <div className="candidacy">
        {
          this.state.candidacies.map( work => {
            return(
              <Box key={work.key}>
                <Media>
                  <Media.Item renderAs="figure" position="left" style={{ backgroundImage: "url(" + work.image + ")" }}>
                    <Image size="3by2"/>
                  </Media.Item>
                  <Media.Item>
                    <Content>
                      <div className="is-size-4 has-text-weight-bold">{work.name}</div>
                      <div className="is-size-6">
                        Telefone: <a href={`tel:${work.phone}`}>{work.phone}</a> |
                        Site: <a href={work.site}>{work.site}</a>
                      </div>
                    </Content>
                    <Level breakpoint="mobile">
                      <Level.Side align="left">
                        <a className="button" href={`/admin/work/${work.key}`}>Ver vaga</a>
                      </Level.Side>
                    </Level>
                  </Media.Item>
                </Media>
              </Box>
            )
          })
        }
        </div>
      </div>
    )
  }
}