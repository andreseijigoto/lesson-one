import React, { Component } from 'react'

import Button from 'react-bulma-components/lib/components/button'
import Columns from 'react-bulma-components/lib/components/columns'
import Heading from 'react-bulma-components/lib/components/heading'

import Input from '$components/Input.jsx'
import Panel from '$components/Panel.jsx'

import firebase from '$utils/firebase.jsx'

import '$sass/views/private-search.scss'

export default class Search extends Component
{
  constructor()
  {
    super()
    this.state = { works: [] }
  }
  componentDidMount()
  {
    firebase.firestore().collection( 'works' ).get().then( querySnapshot => {
      const works = []
      querySnapshot.forEach( doc => {
        const item = doc.data()
        works.push(
        {
          key: doc.id,
          label: item.resume,
          path: '/admin/work/' + doc.id,
          tag: item.type,
          title: item.name
        })
      })
      this.setState({works:works})
    })
  }
  render()
  {
    return (
      <div id="WorkSearch">
        <Heading renderAs="h1">Vagas</Heading>
        <form>
          <Columns>
            <Columns.Column size={8}>
              <Input size="large" type="text" name="region" id="profile-region" label="Região" />
            </Columns.Column>
          </Columns>
          <Columns>
            <Columns.Column size={2}>
              <Input size="large" type="text" name="state" id="profile-state" label="Estado" />
            </Columns.Column>
            <Columns.Column size={4}>
              <Input size="large" type="text" name="city" id="profile-city" label="Cidade" />
            </Columns.Column>
            <Columns.Column size={4}>
              <Input size="large" type="text" name="neighborhood" id="profile-neighborhood" label="Bairro" />
            </Columns.Column>
            <Columns.Column size={2}>
              <div className="button-flex">
                <Button name="search" className="is-info is-large is-pulled-right">Procurar</Button>
              </div>
            </Columns.Column>
          </Columns>
        </form>
        <Panel header={{title:"Últimas vagas"}} items={this.state.works}/>
      </div>
    )
  }
}