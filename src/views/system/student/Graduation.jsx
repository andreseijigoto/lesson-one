import React, { Component } from 'react'

import Button from 'react-bulma-components/lib/components/button'
import Columns from 'react-bulma-components/lib/components/columns'
import Heading from 'react-bulma-components/lib/components/heading'
import Notification from 'react-bulma-components/lib/components/notification'

import GraduationModel from '$models/Graduation.jsx'

import Input from '$components/Input.jsx'

export default class Graduation extends Component
{
  constructor()
  {
    super()
    this.state = {}
    this.userId = sessionStorage.getItem( 'userId' )
    this.saveGraduation = this.saveGraduation.bind( this )
  }
  saveGraduation( e )
  {
   e.preventDefault()
    const fieldList = e.target.elements
    let dataSend = {}
    Object.keys( fieldList ).map( key => {
      dataSend[ fieldList[ key ].name ] = fieldList[ key ].value
      return null
    })
    GraduationModel.setData( this.userId, dataSend, res => {
      if( res.success )
        this.setState({message:'Dados da Graduação salvos com sucesso!'})
      if( res.error )
        this.setState({error:'Não foi possivel salvar os dados. Contacte o Suporte para maiores informações.'})
    })
  }
  componentDidMount()
  {
    GraduationModel.getDataById( this.userId, res => {
      this.setState( res )
    })
  }
  render()
  {
    return (
      <div id="Graduation">
        {
          this.state.message || this.state.error ?
          <Notification color={this.state.message ? 'info' : 'danger'}>
          {this.state.message || this.state.error}<Button remove onClick={ e =>{ this.setState( { error:null } ) } }/>
          </Notification>
          : ''
        }
        <Heading renderAs="h1">Graduação</Heading>
        <form onSubmit={this.saveGraduation}>
          <Columns>
            <Columns.Column size={9}>
              <Input size="large" type="text" name="institution" id="graduation-institution" label="Faculdade" required value={this.state.institution}/>
            </Columns.Column>
            <Columns.Column size={3}>
              <Input size="large" type="text" name="ra" id="graduation-ra" label="RA" required value={this.state.ra}/>
            </Columns.Column>
            <Columns.Column size={12}>
              <Input size="large" type="text" name="course" id="graduation-course" label="Curso" required value={this.state.course}/>
            </Columns.Column>
            <Columns.Column size={4}>
              <Input size="large" type="text" name="current" id="graduation-current" label="Semestre" required value={this.state.current}/>
            </Columns.Column>
            <Columns.Column size={4}>
              <Input size="large" type="text" name="begin" id="graduation-begin" label="Data Início" required value={this.state.begin}/>
            </Columns.Column>
            <Columns.Column size={4}>
              <Input size="large" type="text" name="end" id="graduation-end" label="Data Conclusão (previsão)" required value={this.state.end}/>
            </Columns.Column>
          </Columns>          
          <Button name="save" className="is-info is-large is-pulled-right">Salvar</Button>
        </form>
      </div>
    )
  }
}