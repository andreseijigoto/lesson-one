import React, { Component } from 'react'

import Box from 'react-bulma-components/lib/components/box'
import Button from 'react-bulma-components/lib/components/button'
import Columns from 'react-bulma-components/lib/components/columns'
import Heading from 'react-bulma-components/lib/components/heading'

import Checkbox from '$components/Checkbox.jsx'

import ConfigsModel from '$models/Configs.jsx'

export default class Configs extends Component
{
  constructor()
  {
    super()
    this.state = {}
    this.userId = sessionStorage.getItem( 'userId' )
    this.saveConfigs = this.saveConfigs.bind( this )
  }
  saveConfigs( e )
  {
    e.preventDefault()
    const fieldList = e.target.elements
    let dataSend = {}
    Object.keys( fieldList ).map( key => {
      if( fieldList[ key ].checked !== undefined )
        dataSend[ fieldList[ key ].name ] = fieldList[ key ].checked
      return null
    })
    ConfigsModel.setData( this.userId, dataSend, res => {
      if( res.success )
        this.setState({message:'Dados da Graduação salvos com sucesso!'})
      if( res.error )
        this.setState({error:'Não foi possivel salvar os dados. Contacte o Suporte para maiores informações.'})
      this.componentDidMount()
    })
  }
  componentDidMount()
  {
    ConfigsModel.getDataById( this.userId, res => {
      this.setState( res )
    })
  }
  render()
  {
    return (
      <div id="Configs">
        <Heading renderAs="h1">Configurações</Heading>
        <form onSubmit={this.saveConfigs}>
          <Columns>
            <Columns.Column size={'half'}>
              <Box>
                <Heading renderAs="h2" className="is-size-5">Cadastro</Heading>
                <Checkbox id="PublicProfile" name="publicprofile" checked={this.state.publicprofile} label="Manter meu perfil público"/>
                <Checkbox id="PublicPhone" name="publicphone" checked={this.state.publicphone} label="Manter meus telefone público"/>
                <Checkbox id="PublicEmail" name="publicemail" checked={this.state.publicemail} label="Manter meus e-mail público"/>
              </Box>
              <Box>
                <Heading renderAs="h2" className="is-size-5">E-mail</Heading>
                <Checkbox id="NewestByEmail" name="newestbyemail" checked={this.state.newestbyemail} label="Receber por e-mail sobre novas vagas"/>
                <Checkbox id="MessagesByEmail" name="messagesbyemail" checked={this.state.messagesbyemail} label="Receber por e-mail sobre Novas mensagens"/>
              </Box>
            </Columns.Column>
            <Columns.Column size={'half'}>
              <Box>
                <Heading renderAs="h2" className="is-size-5">Whatsapp</Heading>
                <Checkbox id="NewestByWhatsApp" name="newestbywhatsapp" checked={this.state.newestbywhatsapp} label="Receber por Whatsapp sobre novas vagas"/>
                <Checkbox id="MessagesByWhatsApp" name="messagesbywhatsapp" checked={this.state.messagesbywhatsapp} label="Receber por Whatsapp sobre Novas mensagens"/>
              </Box>
              <Box>
                <Heading renderAs="h2" className="is-size-5">SMS</Heading>
                <Checkbox id="NewestBySms" name="newestbysms" checked={this.state.newestbysms} label="Receber por SMS sobre novas vagas"/>
                <Checkbox id="MessagesBySms" name="messagesbysms" checked={this.state.messagesbysms} label="Receber por SMS sobre Novas mensagens"/>
              </Box>
            </Columns.Column>
          </Columns>
          <Button name="save" className="is-info is-large is-pulled-right">Salvar</Button>
        </form>
      </div>
    )
  }
}