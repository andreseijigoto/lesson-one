import React, { Component, Fragment } from 'react'
import '$sass/views/home.scss'

export default class Home extends Component
{
  constructor()
  {
    super()
    this.modal = {}
    this.imageNumber = Math.floor( Math.random() * 3 ) || 1
  }
  // showRegisterModal()
  // {
  //   PubSub.publish( 'modal-register' )
  // }
  render()
  {
    const imageBg = require( '$images/background/image' + this.imageNumber + '.jpg' )
    return (
      <Fragment>
        <section id="Home" />
        <div className="background" style={{backgroundImage:"url(" + imageBg + ")"}}/>
      </Fragment>
    )
  }
}