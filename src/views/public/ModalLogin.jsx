import React, { Component, Fragment } from 'react'
import { withRouter } from "react-router"

import Button from 'react-bulma-components/lib/components/button'
import Heading from 'react-bulma-components/lib/components/heading'
import Notification from 'react-bulma-components/lib/components/notification'

import Input from '$components/Input.jsx'
import firebase from '$utils/firebase.jsx'
import { createBrowserHistory } from "history"
import '$sass/views/modal-login.scss'

class ModalLogin extends Component
{
  constructor( props )
  {
    super()
    this.state = { error: null }
    this.history = new createBrowserHistory()
    this.signIn = this.signIn.bind( this )
  }
  signIn = async e => {
    e.preventDefault()
    const { username, password } = e.target.elements
    await firebase
      .auth()
      .signInWithEmailAndPassword( username.value, password.value )
      .then( res => {
        this.props.history.push( "/admin/dashboard" )
      }) 
     .catch( error => {
        if( error.code === 'auth/wrong-password' )
          this.setState({error: 'Senha ou e-mail inválido'})
        if( error.code === 'auth/user-not-found' )
          this.setState({error: 'Usuário não encontrado'})
      })
  }
  render()
  {
    return(
      <Fragment>
        <Heading className="has-text-centered">Login</Heading>
        <form id="FormModalLogin" onSubmit={this.signIn}>
          {
            this.state.error ? 
            <Notification color="danger">
            {this.state.error}<Button remove onClick={ e =>{ this.setState( { error:null } ) } }/>
            </Notification>
            : null
          }
          <Input size="large" id="ModalLoginUsername" name="username" type="email" required="required" placeholder="E-mail"/>
          <Input size="large" id="ModalLoginPassword" minlength="6" name="password" type="password" required="required" placeholder="Password"/>          
          <input type="submit" className="is-fullwidth is-large button" value="Logar" />  
          <a href="/recover" className="is-pulled-left">Esqueci minha senha</a>
          <a href="/register" className="is-pulled-right">Cadastrar</a>
        </form>
      </Fragment>
    )
  }
}
export default withRouter( ModalLogin )