import React, { Component } from 'react'
import { createBrowserHistory } from "history"

import Button from 'react-bulma-components/lib/components/button'
import Columns from 'react-bulma-components/lib/components/columns'
import Container from 'react-bulma-components/lib/components/container'
import { Field, Control } from 'react-bulma-components/lib/components/form'
import Heading from 'react-bulma-components/lib/components/heading'
import Input from '$components/Input.jsx'
import Select from '$components/Select.jsx'

import '$sass/views/register.scss'

export default class Register extends Component
{
  constructor()
  {
    super()
    this.history = new createBrowserHistory()
    this.signUp = this.signUp.bind( this )
  }
  signUp = async e => {
    e.preventDefault()    
    const { username, password, profile } = e.target.elements
  }
  render()
  {
    return (
      <section id="Register">
        <Container fluid>
          <Heading renderAs="h1">Dados Pessoais</Heading>
            <Columns>
              <Columns.Column size={8} offset={2}>
                <form id="RegisterForm" onSubmit={this.signUp}>
                  <Select size="large" name="profile" id="profile" label="Perfil" required readonly defaultValue={3}>
                    <option disabled value="1">Instituições de Ensino</option>
                    <option disabled value="2">Faculdades Associadas</option>
                    <option value="3">Estudantes / Recém formados</option>
                  </Select>
                  <Input size="large" type="email" name="username" id="username" label="Email" required />
                  <Input size="large" type="password" name="password" id="password" label="Senha" required />
                  <Field kind="group">
                    <Control>
                      <Button name="search" className="has-background-grey-lighter is-large" onClick={this.history.goBack}>Voltar</Button>
                    </Control>
                    <Control>
                      <Button name="save" className="is-info is-large is-pulled-right">Salvar</Button>
                    </Control>
                  </Field>
                </form>
              </Columns.Column>
            </Columns>
        </Container>
      </section> 
    )
  }
}