import React from 'react'
import ReactDOM from 'react-dom'
import App from '$root/App.jsx'

import '$sass/libraries/bulma.min.scss'
import '$sass/system.scss'

import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(
  <App />,  
  document.getElementById("root")
)

registerServiceWorker()