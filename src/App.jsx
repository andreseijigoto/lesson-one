import React, { Component }from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom"

import firebase from '$utils/firebase.jsx'
import PrivateRoute from "$utils/privateRoute.jsx"

import publicRoute from "$routes/public.jsx"
import privateRoute from "$routes/private.jsx"

import AccountsModel from '$models/Accounts.jsx'

export default class App extends Component
{
  constructor()
  {
    super()
    this.userId = sessionStorage.getItem( 'userId' ) || null
    this.state = {
      loading: true,
      authenticated: sessionStorage.getItem( 'userId' ) ? true : false,
      profile: null
    }
  }
  componentWillMount()
  {
    firebase.auth().onAuthStateChanged( sesUser => {
      if( sesUser )
      {
        sessionStorage.setItem( 'userId', sesUser.uid )
        this.userId = sesUser.uid
      }
      this.setState(
      {
        loading: false,
        authenticated: this.userId ? true : false,
      })
    })
    // if( this.userId )
    //   AccountsModel.getAccess( this.userId, access => {
    //     let routes = require( '$routes/' + access + '.jsx' )
    //     this.setState({privateRoute:routes.default})
    //   })
  }
  render()
  {
    return (
      <BrowserRouter>
        <Switch>
        {
          publicRoute.list.map( ( route, key ) => {
            return(
              <Route exact path={route.path} key={key} render={ () => {
                return <publicRoute.layout config={route} />
              }}/>
            )
          })
        }
        {
          privateRoute.list.map( ( route, key ) => {
            return(
              <PrivateRoute path={route.path} key={key} authenticated={this.state.authenticated}
                config={route} routes={privateRoute.list} layout={privateRoute.layout} />
            )
          })
        }
        </Switch>
      </BrowserRouter>
    )
  }
}