import React from 'react'
import BaseComponent from '$components/_BaseComponent.jsx'
import { Field, Checkbox } from 'react-bulma-components/lib/components/form'

export default class CustomCheckbox extends BaseComponent
{
	constructor()
	{
		super();
		this.state = { name: '', checked: false, label: '' }
		this.setValue = this.setValue.bind( this )
	}
	setValue( evt )
	{
		this.setState( { checked: evt.target.checked } )
	}
	componentDidMount()
	{
		this.setState( this.props )
	}
	componentWillReceiveProps( props )
	{
		this.setState( props )
		if( props.checked )
			document.querySelector( `[name="${this.state.name}"]` ).checked = true
	}
	render()
	{
		return (
      <Field>
	      <Checkbox name={this.state.name} onChange={this.setValue} value="1" checked={this.state.checked}>
	      	{this.state.label}
	      </Checkbox>
      </Field>	
		);
	}
}
