import React from 'react'
import SystemComponent from '$components/_BaseComponent.jsx'
import { Field, Control, Label } from 'react-bulma-components/lib/components/form'

export default class CustomInput extends SystemComponent
{
	constructor()
	{
		super()	
		this.className = 'select'
		this.onChange = this.onChange.bind( this )
		this.state = { name:'', placeholder:'', label:'', value:'', options: [] }
	}
	componentDidMount()
	{
		this.setState( this.props );
	}
	onChange( evt )
	{
		this.setState(
		{
			value: evt.target.value
		})
	}
	render()
	{
		if( this.state.size )
			this.className += ` is-${this.state.size}`
		return (
		  <Field>
        { this.state.label ? <Label>{this.state.label}</Label> : null }
        <Control>
        	<select className={this.getClassName()} onChange={this.onChange} value={this.state.value}>
					{ this.props.children }
          </select>
        </Control>
      </Field>
		)
	}
}
