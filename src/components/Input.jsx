import React from 'react'
import { Field, Control, Label, Input } from 'react-bulma-components/lib/components/form'
import BaseComponent from '$components/_BaseComponent.jsx'

export default class CustomInput extends BaseComponent
{
	constructor( props )
	{
		super( props )
		this.state = { name:'', placeholder:'', label:'', type:'text', value: '', size: 'medium' }
		this.setValue = this.setValue.bind( this )
		console.log( props )
	}
	componentDidMount()
	{
		this.setState( this.props );
	}
	setValue( evt )
	{
		console.log( evt.target )
		this.setState( { value: evt.target.value } )
	}
	render()
	{
		let backgroundImage = ''
		if( this.state.type === 'file' )
			this.className = 'file'
		if( this.state.backgroundImage )
		{
			backgroundImage = {style:{backgroundImage:`url(${this.state.backgroundImage})`}}
			this.className += ' has-background'
		}
		
		return (
			<Field>
        {
        	this.state.label ?
        		<Label className={ this.state.required ? 'required' : null }>
        			{this.state.label}
        		</Label>
        	: null
        }
        <Control className={this.getClassName()} {...backgroundImage}>
        {
        	this.state.type === 'file' ?
      			<input
      				value={this.state.value}
		          placeholder={this.state.placeholder}
		          id={this.state.id}
		          onChange={this.setValue}
	          	type={this.state.type}
	          	name={this.state.name}
	          	size={this.state.size}
	          	required={this.state.required} />
      		:
	          <Input
		          value={this.state.value}
		          placeholder={this.state.placeholder}
		          id={this.state.id}
	          	onChange={this.setValue}
	          	type={this.state.type}
	          	name={this.state.name}
	          	size={this.state.size}
	          	required={this.state.required} 
	          	minLength={this.state.minlength} />
        	}
        </Control>
      </Field>
		);
	}
}
