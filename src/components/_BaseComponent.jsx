import { Component } from 'react'

export default class CustomIcon extends Component
{
	constructor()
	{
		super()
		this.state = { errMsg:'' }
	}
	componentWillReceiveProps( props )
	{
		this.setState( props )
	}
	getClassName( extra )
	{
		let className = [];
		if( extra )
			className.push( extra	 )
		if( this.className )
			className.push( this.className )
		if( this.props.className )
			className.push( this.props.className )
		return className.join( ' ' )
	}
}