import React from 'react'
import Heading from 'react-bulma-components/lib/components/heading'
import Tag from 'react-bulma-components/lib/components/tag'
import BaseComponent from '$components/_BaseComponent.jsx'
import Icon from '$components/Icon.jsx'

export default class CustomPanel extends BaseComponent
{
	constructor()
	{
		super()
		this.state = {
			header: {},
			title: null
		}
		this.className = 'panel'
	}
	componentDidMount()
	{
		this.setState( this.props )
	}
	render()
	{
		return (
			<div className={this.getClassName()}>
				<div className="panel-header">
				{
					this.state.header.icon ?
					<Icon icon={this.state.header.icon} size="lg" className="lg"/>
					: null
				}
				{
					this.state.header.title ? 
					<Heading renderAs="h1">{this.state.header.title}</Heading>
					: null
				}
				</div>
				<div className="panel-content">
				{
					this.state.title ?
					<Heading renderAs="h2" subtitle>{this.state.title}</Heading>
					: null
				}
				{
					this.state.items ?
						<ul>
						{
							this.state.items.map( ( item, key ) => {
								let tag = item.tag ?
									<Tag color={item.tag === 'public' ? 'success' : 'danger'}>
										{item.tag === 'public' ? 'Público' : 'Particular'}
									</Tag>
									: null
								let title = item.title ? <b>{item.title}: </b> : null
								return( <li key={item.key||key}><a href={item.path}>{tag}{title}{item.label}</a></li> )
							})
						}
						</ul>
					: null
				}
				</div>
			</div>
		)
	}
}
