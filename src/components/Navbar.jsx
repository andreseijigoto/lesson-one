import React, { Component } from 'react'
import Button from 'react-bulma-components/lib/components/button'
import Navbar from 'react-bulma-components/lib/components/navbar'
 
export default class CustomNavbar extends Component
{
	constructor()
	{
		super()
		this.state = {
			color:'white',
			transparent:false,
			brand: {},
			start: [],
			end: []
		}
	}
	componentDidMount()
	{
		this.setState(
		{
			color: this.props.color,
			transparent: this.props.transparent,
			brand: this.props.brand || null,
			start: this.props.items.start || [],
			end: this.props.items.end || [],
		})
	}
	render()
	{
		return (
			<Navbar color={this.state.color||"white"} transparent={this.state.transparent}>
			{
				this.state.brand ?
		<Navbar.Brand>
		  <Navbar.Item renderAs="a" href="#">
			<img src={this.state.brand.logo} alt={this.state.brand.label} width={this.state.brand.width}/>
		  </Navbar.Item>
		</Navbar.Brand>
				: null
			}
		<Navbar.Menu>
		{
			this.state.start ?
			<Navbar.Container>
			{
				Object.keys( this.state.start ).map( key => {
					let item = this.state.start[ key ]
					return(
						<Navbar.Item key={key} href={item.path} className={item.className||null}>
						{ item.label }
						</Navbar.Item>
					)
				}) 
			}
			</Navbar.Container>
			: null
		}
		{
			this.state.end ?
			<Navbar.Container position="end">
			{
				Object.keys( this.state.end ).map( key => {
					let item = this.state.end[ key ]
					if( item.path )
						return <a key={key} href={item.path} {...item}>{ item.label }</a>
					else
						return <Button key={key} href={item.path} {...item}>{ item.label }</Button>
				}) 
			}
			</Navbar.Container>
			: null
		}
		</Navbar.Menu>
	  </Navbar>
		)
	}
}