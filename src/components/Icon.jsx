import React from 'react'
import BaseComponent from '$components/_BaseComponent.jsx'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Icon from '$utils/Icons.jsx'

export default class CustomIcon extends BaseComponent
{
	constructor()
	{
		super()
		this.state = { errMsg:'' }
		this.className = 'icon'
		new Icon()
	}
	render()
	{
		let size = this.props.size ? 'fa-' + this.props.size : ''
		return (
			<span className={this.getClassName( size )}>
  	  	<FontAwesomeIcon icon={this.props.icon} alt={this.props.alt}/>
	  	</span>
		)
	}
}