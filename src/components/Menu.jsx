import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { createBrowserHistory } from "history"
import Menu from 'react-bulma-components/lib/components/menu'

export default class CustomMenu extends Component
{
	constructor( props )
	{
		super()
		this.history = new createBrowserHistory()
		this.state = { header:{}, routes: [], current: this.history.location.pathname }
		this.evtCollapsible = this.evtCollapsible.bind( this )
	}
	evtCollapsible( evt )
	{
		evt.preventDefault()
		evt.stopPropagation()
		if( evt.target.classList.contains( 'collapsible' ) )
		{
			if( !evt.target.classList.contains( 'active' ) )
				evt.target.classList.toggle( 'show' )
			evt.target.parentElement.querySelector( 'ul.menu-list' ).classList.toggle( 'slidedown' )
			evt.target.parentElement.querySelector( 'ul.menu-list' ).classList.toggle( 'slideup' )
		}
	}
	checkActiveChild( obj )
	{
		let check = false
		if( obj.list )
			obj.list.map( child => {
				if( child.path === this.state.current )
					check = true
				return false
			})
		return check
	}
	getHeader( header )
	{
		if( header )
			return(
				<header className="brand">
					<a href={header.path}>
						<img className="logo" src={header.logo} alt={header.label} />
					</a>
				</header>
			)
		return null
	}
	getMenu( menu )
	{
		let newMenu = []
		menu.map( ( obj, key ) => {
			if( !obj.group )
				newMenu.push( obj )
			if( obj.group )
			{
				if( obj.groupLabel )
				{
					let group = {}
					group.id = obj.group
					group[ 'sidebarName' ] = obj.groupLabel
					group.list = [ obj ]
					newMenu.push( group )
				}
				if( !obj.groupLabel	)
				{
					Object.keys( newMenu ).map( newKeys => {
						if( newMenu[ newKeys ].id === obj.group )
							newMenu[ newKeys ].list.push( obj )
						return null
					})
				}
			}
			return null
		})
		return this.getMenuList( newMenu )
	}
	getMenuList( menu, label, className )
	{
		return(
			<Menu.List title={label} onClick={this.evtCollapsible} className={className}>
			{
				menu.map( ( obj, key ) => {
					if( obj.sidebarName )
						return( this.getMenuListItem( obj, key ) )
					return false
				})
			}
			</Menu.List>
		)
	}
	getMenuListItem( obj, key )
	{
		let active = this.checkActiveChild( obj )
		let id = obj.id ? 'Menu' + obj.id : null
		return(
			<Menu.List.Item key={key} id={id} className={'collapsible '+(active?'active':'')}>
			{
				obj.list ?
				this.getMenuList( obj.list, obj.sidebarName, active ? 'slidedown' : 'slideup' )
				: this.getNavLink( obj )
			}
			</Menu.List.Item>
		)
	}
	getNavLink( obj )
	{
		let id = obj.id ? 'Menu' + obj.id : null
		let hide = obj.hidden ? {display:'none'} : null

		return(
			<Fragment>
			{
				obj.path ? 
				<NavLink to={obj.path} id={id} className="link" style={hide}>{obj.sidebarName}</NavLink>
				: 
				<a onClick={obj.onClick} id={id} className="link">{obj.sidebarName}</a>
			}
			</Fragment>
		)
	}
	render()
	{
		return(
			<Menu id={ this.props.id || null }>
			{ this.getHeader( this.props.header ) }
			{ this.getMenu( this.props.items ) }
			</Menu>
		)
	}
}