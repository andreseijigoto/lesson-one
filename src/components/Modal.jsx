import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PubSub from 'pubsub-js'
import Modal from 'react-bulma-components/lib/components/modal'
 
export default class CustomModal extends Component
{
	static propTypes = {
    modal: PropTypes.object,
    children: PropTypes.node.isRequired
  }
  constructor( props )
  {
    super()
		this.state = { show: false, component: null }
		this.open = this.open.bind( this )
		this.close = this.close.bind( this )
		PubSub.subscribe( props.pubsub, ( topic, component ) => {
			this.open()
		})
	}
  open()
  {
  	this.setState({ show: true })
  }
  close()
  {
  	this.setState({ show: false })  	
  }
	render()
	{
		return (
			<Modal modal={{ closeOnEsc: true, closeOnBlur: true }} show={this.state.show} onClose={this.close}>
	      <Modal.Content id={this.props.id}>
	      { this.props.children }
	      </Modal.Content>
	    </Modal>
		)
	}
}