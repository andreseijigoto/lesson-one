import React from "react";
import { Route, Redirect } from "react-router-dom";

export default function privateRoute( { config:config, layout: Layout, authenticated, ...rest } )
{
  return (
    <Route
      {...rest}
      render={ props =>
        authenticated === true ? (
          <Route exact path={props.path} render={ () => {
            return <Layout config={config} {...rest} />
          }}/>
        ) : (
          <Redirect to="/" />
        )
      }
    />
  )
}