import { library } from '@fortawesome/fontawesome-svg-core';
import { faExclamation, faQuestion } from '@fortawesome/free-solid-svg-icons';

export default class Icons
{
	constructor( params )
	{
		library.add( faQuestion )
		library.add( faExclamation )
	}
}