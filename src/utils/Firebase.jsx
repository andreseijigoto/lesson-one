import firebase from 'firebase'

const settings = { timestampsInSnapshots: true }

const app = firebase.initializeApp(
{
  apiKey: 'AIzaSyDPGhHDWBxRTvMTZ6T4iAOYZJsZWMJzhsA',
  authDomain: 'lesson-one-86473.firebaseapp.com',
  databaseURL: 'https://lesson-one-86473.firebaseio.com',
  projectId: 'lesson-one-86473',
  storageBucket: 'lesson-one-86473.appspot.com',
  messagingSenderId: '228072289275'
});
app.firestore().settings( settings )

export default app