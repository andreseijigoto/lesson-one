import React, { Component, Fragment } from 'react'
import PubSub from 'pubsub-js'
import Navbar from "$components/Navbar.jsx"
import Modal from "$components/Modal.jsx"
import ModalLogin from '$views/public/ModalLogin.jsx'
import logo from '$images/logo.png'
import '$sass/layouts/public.scss'

export default class System extends Component
{
  constructor()
  {
    super()
    this.showLoginModal = this.showLoginModal.bind( this )
  }
  showLoginModal()
  {
    PubSub.publish( 'modal-login' )
  }
  render()
  {
    const Child = this.props.config.component
    return(
      <Fragment>
        <Navbar color="white" transparent={false}
          brand={{logo:logo, label:"Lesson One", width:100}}
          items={
            {
              start: [
                {path:'#',label:'Home',className:'active'},
                {path:'#',label:'Professores'},
                {path:'#',label:'Escolas'},
                {path:'#',label:'Faculdades'},
                {path:'#',label:'Contato'}
              ],
              end: [
                {path:'/register',label:'Register',className:'button light-blue' },
                {label:'Login', onClick:this.showLoginModal},
              ]
            }
          }
        />
        <Child />
        <Modal show={false} id="ModalLogin" pubsub="modal-login">
          <ModalLogin />
        </Modal>
      </Fragment>
    )
  }
}