import React, { Component, Fragment } from 'react'
import { createBrowserHistory } from "history"

import Columns from 'react-bulma-components/lib/components/columns'
import Section from 'react-bulma-components/lib/components/section'
import firebase from '$utils/firebase.jsx'

import Menu from '$components/Menu.jsx'
import logo from '$images/logo.png'

import '$sass/layouts/system.scss'

export default class System extends Component
{
  constructor( props )
  {
    super()
    this.state = {routes:[]}
    this.onSignOut = this.onSignOut.bind( this )
    this.history = new createBrowserHistory()
  }
  onSignOut()
  {
    firebase.auth().signOut()
      .then( res => {
        console.log( res )
        sessionStorage.removeItem( 'userId' )
        window.location.href = '/'
      })
      .catch( error => {
        console.log( error )
      })
  }
  componentDidMount()
  {
    let signOut = false
    this.props.routes.map( obj => {
      if( obj.sidebarName == 'Sair' )
        signOut = true
    })
    if( !signOut )
      this.props.routes.push({sidebarName:'Sair',onClick:this.onSignOut})
    this.setState({routes:this.props.routes})
  }
  render()
  {
    const Child = this.props.config.component
    const config = this.props.config
    const current = this.history.location.pathname
    return(
      <Fragment>
        <Section id="AdminContent">
          <Menu items={this.state.routes} id="AdminSidebar" header={{logo:logo, label:"Lesson One", path:'/admin/dashboard'}}/>
          <Columns>
            <Columns.Column size={10} offset={1}>
              <Child config={config} routes={this.state.routes} current={current}/>
            </Columns.Column>
          </Columns>
        </Section>
      </Fragment>
    )
  }
}  