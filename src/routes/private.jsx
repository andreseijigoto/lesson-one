import Layout from "$layouts/System.jsx"

import PageCandidacy from '$views/system/student/Candidacy.jsx'
import PageGraduation from '$views/system/student/Graduation.jsx'
import PageConfigs from '$views/system/student/Configs.jsx'
import PageDashboard from '$views/system/student/Dashboard.jsx'
import PageProfile from '$views/system/student/Profile.jsx'
import PageSearch from '$views/system/student/Search.jsx'
import PageWork from '$views/system/student/Work.jsx'

const privateRoutes = {
  layout: Layout,
  list: [
    {
      path: '/admin/dashboard',
      sidebarName: 'Dashboard',
      component: PageDashboard,
    },
    {
      path: '/admin/profile',
      sidebarName: 'Dados Pessoais',
      component: PageProfile,
      id: 'Profile',
      group: 'Personal',
      groupLabel: 'Meus Dados'
    },
    {
      path: '/admin/college',
      sidebarName: 'Graduação',
      component: PageGraduation,
      id: 'Graduation',
      group: 'Personal'
    },
    {
      path: '/admin/search',
      sidebarName: 'Pesquisar',
      component: PageSearch,
      id: 'Search',
      group: 'Works',
      groupLabel: 'Vagas'
    },
    {
      path: '/admin/work/:id',
      component: PageWork,
      sidebarName: 'Vaga Selecionada',
      id: 'Selected',
      group: 'Works',
      hidden: true
    },
    {
      path: '/admin/candidacy',
      sidebarName: 'Candidaturas',
      component: PageCandidacy,
      id: 'Candidacy',
      group: 'Works'
    },
    {
      path: "/admin/configs",
      sidebarName: "Configurações",
      component: PageConfigs,
      id: 'Config',
    }
  ]
}
export default privateRoutes