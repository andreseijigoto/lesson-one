import Layout from "$layouts/Public.jsx"
import PageHome from "$views/public/Home.jsx"
import PageRegister from "$views/public/Register.jsx"

const routes = {
  layout: Layout,
  list: [
  {
    path: "/home",
    component: PageHome,
  },
  {
    path: "/register",
    component: PageRegister,
  },
  { 
    redirect: true,
    path: "/",
    component: PageHome,
  }
  ]
}
export default routes
