import Base from '$models/_Base.jsx'

export default class Works extends Base
{
	static getLatest( callback )
	{
		this.getData( ( data ) => {
			let latest = []
			data.map( ( work, key ) => {
				latest.push(
        {
          key: work.key,
          label: work.resume,
          path: '/admin/work/' + work.key,
          tag: work.type,
          title: work.name
        })
        return null
			})
			callback( latest )
		})
	}
}