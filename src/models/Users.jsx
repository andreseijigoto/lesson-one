import Base from '$models/_Base.jsx'

export default class Users extends Base
{
	static getProfile( id, callback )
	{
		this.getDataById( id, user => { 
			callback( user )
		})
	}
	static setProfile( id, data, callback )
	{
		this.uploadFile( { folder: 'profile', file: data.photo, name: id }, res => {
			if( res.success )
				data.photo = res.url
			this.setData( id, data, res => {
				callback( res )
			})
		})
	}
}