import Base from '$models/_Base.jsx'

export default class Candidacy extends Base
{
	static getCandidacyApplied( userId, workId, callback )
	{
		this.getDataById( userId, data => {
			let candidacyApplied = true
			Object.keys( data ).map( candidacy => {
				if( data[ candidacy ].id === workId )
					candidacyApplied = false
				return null
			})
			return callback( candidacyApplied )
		})
	}
	static setCandidacyApply( userId, workDoc, callback )
	{
		this.getDataById( userId, userData => {
		 	this.setData( userId, { ...userData, ...workDoc }, res => {
		 		callback( res )
		 	})
		})
	}
	static getCadidacyList( userId, callback )
	{
		let candidacies = []
		this.getFirebase().doc( userId ).get().then( candidacyList => {
			let promises = []
			candidacyList = candidacyList.data()
			Object.keys( candidacyList ).forEach( candidacyKey => {
				const request = candidacyList[ candidacyKey ].get().then( candidacy => {
					let candidacyData = candidacy.data()
					candidacyData.key = candidacy.id
					candidacies.push( candidacyData )
				})
				promises.push( request )
			})
			return Promise.all( promises )
		}).then( () => callback( candidacies ) )
	}
}