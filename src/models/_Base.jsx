import firebase from '$utils/firebase.jsx'

export default class Base
{
	static getFirebase()
	{
		return firebase.firestore().collection( this.name.toLowerCase() )
	}
	static getData( callback )
	{
		this.getFirebase().get()
			.then( querySnapshot => {
				let data = []
				querySnapshot.forEach( doc => {
	        const item = doc.data()
	        item.key = doc.id
	        data.push( item )
	      })
	      callback( data )
			})
	}
	static getDataById( id, callback )
	{		
		this.getFirebase().doc( id ).get()
			.then( doc => {
				callback( doc.data() )
			})
	}
	static getDocById( id, callback )
	{		
		callback( this.getFirebase().doc( id ) )
	}
	static setData( id, data, callback )
	{
		this.getFirebase().doc( id ).set( data )
			.then( () => {
				callback({success:true})
			})
			.catch( error => {
				callback({error:error})
			})
	}
	static uploadFile( data, callback )
	{
		let storageRef = firebase.storage().ref()
		let uploadTask = storageRef.child( `${data.folder}/${data.name}` ).put( data.file )
		uploadTask.on( 'state_changed',
			snapshot => {
				var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
			},
			error => {
			},
			success => {
				uploadTask.snapshot.ref.getDownloadURL().then( downloadURL => {
					callback( { success:true, url: downloadURL } )
				})
			}
		)
	}
}