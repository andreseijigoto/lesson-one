import Base from '$models/_Base.jsx'

export default class Accounts extends Base
{
	static getAccess( id, callback )
	{
		this.getDataById( id, account => { 
			callback( account.profile )
		})
	}
}